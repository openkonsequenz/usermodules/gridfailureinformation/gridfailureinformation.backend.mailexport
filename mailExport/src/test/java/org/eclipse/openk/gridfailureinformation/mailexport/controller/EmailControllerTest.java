/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.mailexport.controller;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import org.eclipse.openk.gridfailureinformation.mailexport.MailExportApplication;
import org.eclipse.openk.gridfailureinformation.mailexport.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.mailexport.service.EmailService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import jakarta.mail.MessagingException;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = MailExportApplication.class)
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
public class EmailControllerTest {
    @Autowired
    private WebApplicationContext context;

    private static GreenMail mailServer;

    @Autowired
    @SpyBean
    private EmailService emailService;

    private static MockMvc mockMvc;

    @BeforeAll
    public static void beforeAll() {
        ServerSetup serverSetup = new ServerSetup(3025 , "localhost", ServerSetup.PROTOCOL_SMTP);
        serverSetup.setServerStartupTimeout(3000);
        mailServer = new GreenMail(serverSetup);
    }

    @BeforeEach
    public void init() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
        mailServer.start();
    }

    @AfterEach
    public void afterTest() {
        mailServer.stop();
    }

    @Test
    @WithMockUser(value = "mockedUser")
    public void shouldReturnOkOnSend() throws Exception {
        mockMvc.perform(post("/mail")
                .contentType(MediaType.APPLICATION_JSON)
                .content("test@test.de"))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    @WithMockUser(value = "mockedUser")
    public void shouldReturnStatus() throws Exception {
        doThrow(new MessagingException()).when(emailService).sendTestMail(any());
        mockMvc.perform(post("/mail")
                .contentType(MediaType.APPLICATION_JSON)
                .content("test@test.de")
        ).andExpect(status().is5xxServerError());
    }

    @Test
    public void shouldReturnUnauthorized() throws Exception {
        mockMvc.perform(post("/mail")
                .contentType(MediaType.APPLICATION_JSON)
                .content("test@test.de"))
                .andExpect(status().isUnauthorized());
    }
}
