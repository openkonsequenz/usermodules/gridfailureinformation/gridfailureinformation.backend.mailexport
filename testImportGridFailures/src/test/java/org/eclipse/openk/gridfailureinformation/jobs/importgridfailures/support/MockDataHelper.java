/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.support;

import org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.dtos.ForeignFailureMessageDto;

public class MockDataHelper {

    private MockDataHelper() {}

    public static ForeignFailureMessageDto mockForeignFailureDto() {
        ForeignFailureMessageDto foreignFailureMessageDto = new ForeignFailureMessageDto();
        foreignFailureMessageDto.setMetaId("XYZ_111_gdfr");
        foreignFailureMessageDto.setSource("FremdsystemXY");
        foreignFailureMessageDto.setDescription("Rohrbruch");
        foreignFailureMessageDto.setPayload(null);

        return foreignFailureMessageDto;
    }
}
