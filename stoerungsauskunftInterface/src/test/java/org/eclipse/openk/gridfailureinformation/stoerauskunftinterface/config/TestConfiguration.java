/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.config;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import feign.Request;
import feign.RequestTemplate;
import feign.Response;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.StoerungsauskunftInterfaceApplication;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.api.StoerungsauskunftApi;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.config.rabbitMq.RabbitMqConfig;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.dtos.StoerungsauskunftOutage;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.dtos.StoerungsauskunftUserNotification;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.mapper.StoerungsauskunftMapper;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.mapper.StoerungsauskunftMapperImpl;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.service.ImportExportService;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.support.MockDataHelper;
import org.mapstruct.factory.Mappers;
import org.springframework.amqp.rabbit.connection.Connection;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.test.TestRabbitTemplate;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.messaging.MessageChannel;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

@EntityScan(basePackageClasses = StoerungsauskunftInterfaceApplication.class)
public class TestConfiguration {
    @Bean
    public ObjectMapper objectMapper() { return new ObjectMapper(); }

    @Bean
    public StoerungsauskunftMapperImpl stoerungsauskunftMapper() {return new StoerungsauskunftMapperImpl();}

    @Bean
    public MessageChannel failureImportChannel() {return mock(MessageChannel.class);}

    @Bean
    public RabbitMqConfig rabbitMqConfig() {
        RabbitMqConfig rabbitMqConfigMock = mock(RabbitMqConfig.class);
        doNothing().when(rabbitMqConfigMock).buildAllQueues();
        return rabbitMqConfigMock;
    }

    @Bean
    public TestRabbitTemplate template() {
        return new TestRabbitTemplate(connectionFactory());
    }

    @Bean
    public ConnectionFactory connectionFactory() {
        ConnectionFactory factory = mock(ConnectionFactory.class);
        Connection connection = mock(Connection.class);
        Channel channel = mock(Channel.class);
        willReturn(connection).given(factory).createConnection();
        willReturn(channel).given(connection).createChannel(anyBoolean());
        given(channel.isOpen()).willReturn(true);
        return factory;
    }

    @Bean
    public Response testResponse() {
        var headers = new HashMap<String, Collection<String>>();
        var template = new RequestTemplate();
        var request = Request.create(Request.HttpMethod.GET, "/test", headers, Request.Body.empty(), template);
        return Response.builder().request(request).build();
    }

    @MockBean
    private StoerungsauskunftApi stoerungsauskunftApi;

    @Bean
    public ImportExportService importExportService() {
        return new ImportExportService(
                stoerungsauskunftApi,
                failureImportChannel(),
                objectMapper(),
                Mappers.getMapper(StoerungsauskunftMapper.class)
        );
    }
}
