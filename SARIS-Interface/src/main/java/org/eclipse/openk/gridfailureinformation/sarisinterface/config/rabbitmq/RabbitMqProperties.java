package org.eclipse.openk.gridfailureinformation.sarisinterface.config.rabbitmq;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "spring.rabbitmq", ignoreUnknownFields = true)
public class RabbitMqProperties {

    private String host;
    private String port;
    private String username;
    private String password;

    private String importExchange;
    private String importQueue;
    private String importKey;

}







