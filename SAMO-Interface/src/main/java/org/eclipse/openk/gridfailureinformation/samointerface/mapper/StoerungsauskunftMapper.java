/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.samointerface.mapper;

import org.eclipse.openk.gridfailureinformation.samointerface.constants.Constants;
import org.eclipse.openk.gridfailureinformation.samointerface.dtos.ForeignFailureDataDto;
import org.eclipse.openk.gridfailureinformation.samointerface.dtos.StoerungsauskunftUserNotification;
import org.mapstruct.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface StoerungsauskunftMapper {

    final Pattern STREET_HOUSENUMBER_PATTERN = Pattern.compile("(^\\D+)(\\d.*)");

    @Mappings({
            @Mapping(target = "description", source = "comment"),
            @Mapping(target = "failureBegin", source = "date", dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", qualifiedByName = "convertToUTC"),
            @Mapping(target = "longitude", source = "lng"),
            @Mapping(target = "latitude", source = "lat"),
            @Mapping(target = "street", source = "street", qualifiedByName = "streetMapperToForeignFailureDataDto"),
            @Mapping(target = "housenumber", source = "houseNo"),
    })
    ForeignFailureDataDto toForeignFailureDataDto(StoerungsauskunftUserNotification srcEntity);


    @AfterMapping
    default void afterMappingProcess(StoerungsauskunftUserNotification srcEntity, @MappingTarget ForeignFailureDataDto targetEntity){
        if (targetEntity.getHousenumber() == null || targetEntity.getHousenumber().isEmpty()){
            targetEntity.setHousenumber(housenumberMapperToForeignFailureDataDto(srcEntity.getStreet()));
        }
        targetEntity.setRadiusInMeters(0L);
        targetEntity.setVoltageLevel(Constants.VOLTAGE_LVL_LOW);
    }

    @Named("streetMapperToForeignFailureDataDto")
    default String streetMapperToForeignFailureDataDto(String streetFromUserNotification) {
        if (streetFromUserNotification == null || streetFromUserNotification.isEmpty()) return "";
        String street = streetFromUserNotification;
        Matcher matcher = STREET_HOUSENUMBER_PATTERN.matcher(streetFromUserNotification);
        if (matcher.matches()) {
            street = matcher.group(1).trim();
        }
        return street;
    }

    @Named("housenumberMapperToForeignFailureDataDto")
    default String housenumberMapperToForeignFailureDataDto(String streetFromUserNotification) {
        if (streetFromUserNotification == null || streetFromUserNotification.isEmpty()) return "";
        String housenumber = "";
        Matcher matcher = STREET_HOUSENUMBER_PATTERN.matcher(streetFromUserNotification);
        if (matcher.matches()) {
            housenumber = matcher.group(2).trim();
        }
        return housenumber;
    }

    @Named("convertToUTC")
    default ZonedDateTime convertToUTC(String date) {
        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("d.M.yyyy HH:mm:ss");

        // assume it's in Europe/Berlin timezone
        LocalDateTime ldt = LocalDateTime.parse(date, inputFormatter);

        // return converted value of utc
        return ldt.atZone(ZoneId.of("Europe/Berlin")).withZoneSameInstant(ZoneOffset.UTC);
    }
}
