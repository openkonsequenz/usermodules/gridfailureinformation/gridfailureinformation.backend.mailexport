#!/bin/sh
echo ------- Login Keycloak -------
sh kcadm.sh config credentials --server http://localhost:8380/auth --realm master --user admin --password admin
realmVar="OpenKRealm"
# ***************** CREATING NEW USER *****************
usernameVar="berta_qp"
echo ------- Creating User: $usernameVar -------
sh kcadm.sh create users -s username=$usernameVar -s firstName=Berta -s lastName=QP -s enabled=true -r $realmVar
sh kcadm.sh set-password -r $realmVar --username $usernameVar --new-password $usernameVar
echo pwd set
sh kcadm.sh add-roles --uusername $usernameVar --rolename grid-failure-access --rolename grid-failure-qualifier --rolename grid-failure-publisher -r $realmVar
echo roles set
# ***************** CREATING NEW USER *****************
usernameVar="rudi_qe"
echo ------- Creating User: $usernameVar -------
sh kcadm.sh create users -s username=$usernameVar -s firstName=Rudi -s lastName=QE -s enabled=true -r $realmVar
sh kcadm.sh set-password -r $realmVar --username $usernameVar --new-password $usernameVar
echo pwd set
sh kcadm.sh add-roles --uusername $usernameVar --rolename grid-failure-access --rolename grid-failure-qualifier --rolename grid-failure-creator -r $realmVar
echo roles set
# ***************** CREATING NEW USER *****************
usernameVar="lupin_qpe"
echo ------- Creating User: $usernameVar -------
sh kcadm.sh create users -s username=$usernameVar -s firstName=Lupin -s lastName=QPE -s enabled=true -r $realmVar
sh kcadm.sh set-password -r $realmVar --username $usernameVar --new-password $usernameVar
echo pwd set
sh kcadm.sh add-roles --uusername $usernameVar --rolename grid-failure-access --rolename grid-failure-qualifier --rolename grid-failure-publisher --rolename grid-failure-creator -r $realmVar
echo roles set
# ***************** CREATING NEW USER *****************
usernameVar="alexa_pe"
echo ------- Creating User: $usernameVar -------
sh kcadm.sh create users -s username=$usernameVar -s firstName=Alexa -s lastName=PE -s enabled=true -r $realmVar
sh kcadm.sh set-password -r $realmVar --username $usernameVar --new-password $usernameVar
echo pwd set
sh kcadm.sh add-roles --uusername $usernameVar --rolename grid-failure-access --rolename grid-failure-creator --rolename grid-failure-publisher -r $realmVar
echo roles set
# ***************** CREATING NEW USER *****************
usernameVar="delia_qpea"
echo ------- Creating User: $usernameVar -------
sh kcadm.sh create users -s username=$usernameVar -s firstName=Delia -s lastName=QPEA -s enabled=true -r $realmVar
sh kcadm.sh set-password -r $realmVar --username $usernameVar --new-password $usernameVar
echo pwd set
sh kcadm.sh add-roles --uusername $usernameVar --rolename grid-failure-access --rolename grid-failure-qualifier --rolename grid-failure-publisher --rolename grid-failure-creator --rolename grid-failure-admin -r $realmVar
echo roles set

echo ------- Finished -------
