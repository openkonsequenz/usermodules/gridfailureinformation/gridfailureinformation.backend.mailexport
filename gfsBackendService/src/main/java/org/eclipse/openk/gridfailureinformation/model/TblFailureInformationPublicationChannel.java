/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table( name="tbl_failinfo_pub_channel")
public class TblFailureInformationPublicationChannel {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,  generator = "tbl_fi_pub_channel_seq")
    @SequenceGenerator(name = "tbl_fi_pub_channel_seq", sequenceName = "tbl_fi_pub_channel_seq", allocationSize = 1)
    @Column(name = "id", updatable = false)
    private Long id;

    @ManyToOne
    @JoinColumn( name = "fkTblFailureInformation")
    private TblFailureInformation tblFailureInformation;
    private String publicationChannel;
    private boolean published;


}
